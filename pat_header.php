<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta name="description" content="Patrick's AirWatch Tools">
	<meta name="author" content="Patrick Pulfer">

	<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://pweb.solutions/repositories/mdb432/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://pweb.solutions/repositories/mdb432/css/mdb.css">

	<link rel="shortcut icon" href="https://pweb.solutions/repositories/favicon.ico" type="image/x-icon">
	<link rel="icon" href="https://pweb.solutions/repositories/favicon.ico" type="image/x-icon">

	<!--[if lt IE 9]>
	<script src="https://cdnjs.cloudflare.om/ajax/libs/html5shiv/3.7.2/html5shiv.js"></script>
	<![endif]-->

	<title>
		Patrick's AirWatch environment finder
	</title>
</head>