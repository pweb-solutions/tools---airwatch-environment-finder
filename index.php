<!DOCTYPE HTML>

<!--

**************************************
Live Version

NO CUSTOMER DATA WILL EVER BE RECORDED!

**************************************

-->

<html>

<?php
	
	const AWHealth_URL = "/DeviceServices/awhealth/v1";
		
	include './pat_header.php';
	
/*
error_reporting(E_ALL); 
ini_set('display_errors', 1);
*/

function getVersion($url){
	$server_url = 'https://'.$url.'/AirWatch/AwBase/About';
	$content = file_get_contents($server_url);
	$first_step = explode( '<span>Version:</span><strong>' , $content );
	$second_step = explode("</strong>" , $first_step[1] );
	return $second_step[0];
}
	
function getDomain($email){
	$domain_name = substr(strrchr($email, "@"), 1);
	return $domain_name;
}


?>

<body style="background-color: #212121; color:white;">

<nav class="navbar fixed-top navbar-toggleable-md scrolling-navbar navbar-dark bg-primary">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<a class="navbar-brand" href="#">
		<strong>Patrick's AirWatch environment finder</strong>
	</a>

	<div class="collapse navbar-collapse" id="navbarNav1">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
				<a class="nav-link" href="https://pweb.solutions/tools/airwatch/"><i class="fa fa-eraser" aria-hidden="true"></i>Clear/Reset</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="mailto:ppulfer@vmware.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>Feedback</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="https://pweb.solutions/tools/"><i class="fa fa-home" aria-hidden="true"></i>Hub</a>
			</li>
		</ul>
	</div>
</nav>
	
<br><br><br><br>

<div class="container">
	<div class="row">

		<div class="col-sm-6">
			<div class="card wow fadeInLeft" style="background-color: #2E2E2E; color:white;">
				<div class="card-block">
					<h4 class="card-title" style="color:#33b5e5;">Query</h4>
					<p class="card-text">Search environment details by using AirWatch AutoDiscovery.</p>
					<form action="" method="post" enctype="multipart/form-data">
						Enter the e-mail address here: <input type="email" name="email" id="email" size="60" style="color:white;"/>
						<input type="submit" value="Go!" class="btn btn-success btn-rounded" />
					</form>
				</div>
			</div>
		</div>

<?php

if($_POST["email"] || $_GET["e"]) {
	
	if($_POST["email"]){
		$domain_name= getDomain($_POST["email"]);
		$pat_mail= $_POST["email"];
	}
	elseif($_GET["e"]){
		$domain_name= getDomain($_GET["e"]);
		$pat_mail= $_GET["e"];
	}

	
	$url = "https://discovery.awmdm.com/Autodiscovery/awcredentials.aws/v2/domainlookup/domain/".$domain_name;
	$json = file_get_contents($url);
	$json_data = json_decode($json, true);

	$enrollment_url2 = $json_data[EnrollmentUrl]; //json to string
	$group_id = $json_data[GroupId]; //json to string
	$enrollment_url = substr($enrollment_url2 ,8); //remove https://
	
	$awhealth = $enrollment_url2.AWHealth_URL;
	$json = file_get_contents($awhealth);
	$json_data = json_decode($json, true);
	$awversion = $json_data[Version];
	
	
	$re = '/[^\/]*/'; //remove all until first /
	preg_match($re, $enrollment_url , $matches, PREG_OFFSET_CAPTURE, 0); 

	$domain = $matches[0][0]; //getting aw domain
	
//Get Console Login URL:		
	$ch = curl_init();
	$curl_url = "https://".$domain;
        $timeout = 0;
        curl_setopt ($ch, CURLOPT_URL, $curl_url );
        curl_setopt ($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        curl_setopt($ch, CURLOPT_HEADER, TRUE);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        $header = curl_exec($ch);
        $console_url = curl_getinfo($ch, CURLINFO_EFFECTIVE_URL);
        
       
	$console_url = substr($console_url,8); //remove https://
	
	
	$re = '/[^\/]*/'; //remove all until first /
	preg_match($re, $console_url, $new_matches, PREG_OFFSET_CAPTURE, 0); 

//Parse Console URL
	$console_url = $new_matches[0][0]; //getting new console domain
	if($console_url=="ttps:"){$console_url="";}
	
//Get Console version
	if($awversion==""){
		$pat_console_version = '<font color="red" class="animated flash infinite">BLOCKED</font>';
		$pat_console_version2 = 'BLOCKED';
	}
	else{
		$pat_console_version = '<font color="green"><b>'.$awversion.'</b></font>';
		$pat_console_version2 = $awversion;
		}
	
//Get Console availability
	if(getVersion($console_url)==""){
		$pat_console_availability = '<font color="red">On-Premise</font>';
		$pat_console_availability2 = 'On-Premise';
	}
	else{
		$pat_console_availability = '<font color="green"><b>SaaS or Public</b></font>';
		$pat_console_availability2 = 'SaaS or Public';
	}
	
//Parse Enrollment URL
	if($domain==""){
		$pat_enrollment_url = '<font color="red">AutoDiscovery not set!</font>';
		$pat_enrollment_url2 = 'AutoDiscovery not set!';
	}
	else {
		$pat_enrollment_url = '<font color="green"><b>'.$domain.'</b></font>';
		$pat_enrollment_url2 = $domain;
	}
	
//Parse Group ID
	if($group_id==""){
		$pat_group_id = '<font color="red">AutoDiscovery not set!</font>';
		$pat_group_id2 = 'AutoDiscovery not set!';
	}
	else {
		$pat_group_id = '<font color="green"><b>'.$group_id.'</b></font>';
		$pat_group_id2 = $group_id;
	}
	
//Parse Enrollment URL
	if($console_url==""){
		$pat_console_url = '<font color="red">Unable to resolve</font>';
		$pat_console_url2 = 'Unable to resolve';
	}
	else {
		$pat_console_url = '<font color="green"><b>'.$console_url.'</b></font>';
		$pat_console_url2 = $console_url;
	}
	
  echo '
		<div class="col-sm-6">
			<div class="card wow fadeInRight" style="background-color: #2E2E2E; color:white;">
    		<div class="card-block">
	        <h4 class="card-title" style="color:#33b5e5;">Result</h4>';
	
    	echo "<br>Email: ".$pat_mail;
     	echo "<br>Email Domain: ".$domain_name;
     	echo "<br>";
     	echo "<br>Enrollment URL: ".$pat_enrollment_url;
			echo "<br>Group ID: ".$pat_group_id;
			echo "<br>";
     	echo "<br>Console URL: ".$pat_console_url;
      echo "<br>AirWatch version: ".$pat_console_version;
			echo "<br>";	
			echo "<br>Environment Location: ".$pat_console_availability;
			echo "<br><br>";
      echo '<button class="btn btn-primary btn-rounded" data-clipboard-text="** Environment Information **

Email: '.$_POST["email"].'
Email Domain: '.$domain_name.'
Enrollment URL: '.$pat_enrollment_url2.'
Group ID: '.$pat_group_id2.'
Console URL: '.$pat_console_url2.'
AirWatch version: '.$pat_console_version2.'
Environment Location: '.$pat_console_availability2.'"><i class="fa fa-clipboard" aria-hidden="true"></i> Copy</button>';
      echo "</div></div></div>";
}

?>

</div>
<br>
	
<div class="row">
	<div class="col-sm-12">
		<div class="card wow fadeInUpBig" style="background-color: #2E2E2E; color:white; font-size:10px;">
			<div class="card-block">
				<p class="card-text">Disclaimer: All data is private, confidential and fully encrypted. Results or email addresses ARE NOT recorded.
				</p>
				</form>
			</div>
		</div>
	</div>
</div>
	
</div> <!-- container -->
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-97345234-2', 'auto');
  ga('send', 'pageview');
</script>

<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/tether.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/mdb.js"></script>
<script type="text/javascript" src="https://cdn.rawgit.com/zenorocha/clipboard.js/v1.7.1/dist/clipboard.min.js"></script>
	<script> new Clipboard('.btn'); </script>
	
<script>
  new WOW().init();
</script>
	
</body>
</html>
