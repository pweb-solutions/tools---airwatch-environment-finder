<!DOCTYPE HTML>

<!--

**************************************
Alpha Version

**************************************

-->

<html>

<?php
	include '../pat_header.php';
	
/*
error_reporting(E_ALL); 
*/
ini_set('display_errors', 0);

?>

<body style="background-color: #212121; color:white;">

<nav class="navbar fixed-top navbar-toggleable-md scrolling-navbar navbar-dark bg-primary">
	<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarNav1" aria-controls="navbarNav1" aria-expanded="false" aria-label="Toggle navigation">
		<span class="navbar-toggler-icon"></span>
	</button>

	<a class="navbar-brand" href="#">
		<strong>Patrick's AirWatch API Test</strong>
	</a>

	<div class="collapse navbar-collapse" id="navbarNav1">
		<ul class="navbar-nav ml-auto">
			<li class="nav-item">
					<a class="nav-link smooth-scroll" href="#"><i class="fa fa-angle-double-up" aria-hidden="true"></i>Scroll Up</a>
				</li>
				<li class="nav-item">
					<a class="nav-link smooth-scroll" href="#bottom"><i class="fa fa-angle-double-down" aria-hidden="true"></i>Scroll Down</a>
				</li>
			<li class="nav-item">
				<a class="nav-link" href="https://pweb.solutions/tools/airwatch/api"><i class="fa fa-eraser" aria-hidden="true"></i>Clear/Reset</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="mailto:ppulfer@vmware.com"><i class="fa fa-envelope-o" aria-hidden="true"></i>Feedback</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="https://pweb.solutions/tools/"><i class="fa fa-home" aria-hidden="true"></i>Hub</a>
			</li>
		</ul>
	</div>
</nav>
	
<br><br><br><br>

<div class="container">
	<div class="row">
		
		<div class="col-sm-6">
			<div class="card wow fadeInLeft" style="background-color: #2E2E2E; color:white;">
				<div class="card-block">
					<h4 class="card-title" style="color:#00C851;">About</h4>
					<p class="card-text">AirWatch provides a collection of RESTful APIs that allow external programs to use the core product functionality by integrating the APIs with existing IT infrastructures and third-party applications</p>
					<p class="card-text">Use this tool to test or troubleshoot your API query. It provides a simplified approach by just entering the necessary info, rest is done in the background.</p>
					<p class="card-text">If the result comes empty, it means that the query has failed.</p>
					<br>
					<p class="card-text"><i class="fa fa-exclamation-triangle animated" style="color:orange;" aria-hidden="true"></i> Disclaimer: All data is private, confidential and fully encrypted end-to-end. Any input or the result is NOT recorded!</p>
				</div>
			</div>
		</div>

		<div class="col-sm-6">
			<div class="card wow fadeInRight" style="background-color: #2E2E2E; color:white;">
				<div class="card-block">
					<h4 class="card-title" style="color:#00C851;">Query</h4>
					<br>
					<form action="" method="post" enctype="multipart/form-data">
						<div class="container-fluid">
							<div class="md-form">
							<i class="fa fa-link prefix"></i>
							<input type="text" id="endpoint" name="endpoint" class="form-control" required style="color:#FFFFFF;">
							<label for="endpoint">API Endpoint</label>
						</div>
						<div class="md-form">
							<i class="fa fa-building prefix"></i>
							<input type="text" id="awtenant" name="awtenant" class="form-control" required style="color:#FFFFFF;">
							<label for="awtenant">AirWatch Tenant ID</label>
						</div>
						<div class="md-form">
							<i class="fa fa-user prefix"></i>
							<input type="text" id="username" name="username" class="form-control" required style="color:#FFFFFF;">
							<label for="username">Admin Username</label>
						</div>
						<div class="md-form">
							<i class="fa fa-lock prefix"></i>
							<input type="password" id="password" name="password" class="form-control" required style="color:#FFFFFF;">
							<label for="password">Admin Password</label>
						</div>
						<input type="submit" value="Go!" class="btn btn-success btn-rounded" />
							</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	</div>
	<br>

<?php

if($_POST["endpoint"]) {
	
// Parse Posts
		$curl_endpoint = filter_var($_POST["endpoint"], FILTER_SANITIZE_URL);
		$aw_tenant = filter_var($_POST["awtenant"], FILTER_SANITIZE_STRING);
		$login = filter_var($_POST["username"], FILTER_SANITIZE_STRING);
		$passcode = filter_var($_POST["password"], FILTER_SANITIZE_STRING);
	
// cURL
	$ch = curl_init();
	curl_setopt_array($ch, array(
		CURLOPT_URL => $curl_endpoint,
		CURLOPT_RETURNTRANSFER => TRUE,
		CURLOPT_VERBOSE => TRUE,
		CURLOPT_HEADER => TRUE,
		CURLOPT_HTTPHEADER => array(
					'Accept: application/json;version=1',
        	'Authorization: Basic '.base64_encode("$login:$passcode"),
	        'aw-tenant-code: '.$aw_tenant,
		),
	));

// Run cURL
		$response = curl_exec($ch);
		if(!curl_exec($ch)){
	    die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
		}
		else{
			$header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
			$header = substr($response, 0, $header_size);
			$body2 = substr($response, $header_size);	
			
			$body = json_decode($body2, JSON_PRETTY_PRINT);
		}
	
// Close cURL
		curl_close($ch);


  echo '
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-8">
				<div class="card wow fadeInUp" style="background-color: #2E2E2E; color:white;">
	    		<div class="card-block">
		        <h4 class="card-title" style="color:#00C851;">API Response</h4>';
		
echo '<hr /><pre style="color:#FFFFFF;">';
print_r($body);
echo "</pre>";
echo "</div></div></div>";
	
echo '
<div class="col-md-4">
				<div class="card wow fadeInUp" style="background-color: #2E2E2E; color:white;">
	    		<div class="card-block">
		        <h4 class="card-title" style="color:#00C851;">Header</h4>';
	echo '
						<hr /><p style="color:#33b5e5;">'.$curl_endpoint;
		
echo '<pre style="color:#FFFFFF;">';
print_r($header);
echo "</pre>";
echo "</div></div></div>";
	
echo "</div></div>";
	
	
	
	
echo '
		<footer class="page-footer dark bg-primary center-on-small-only wow bounceInUp">
			<div class="container-fluid">
					<div class="row"></div>
			</div>
			<div class="footer-copyright" id="bottom">
					<div class="container-fluid">
							© Coded by: <a href="mailto:patrick@pweb.solutions"> PWeb.Solutions </a>
					</div>
			</div>
		</footer>
';
	
}

?>
	
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-97345234-3', 'auto');
  ga('send', 'pageview');
</script>

<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/jquery-2.2.3.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/tether.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://pweb.solutions/repositories/mdb432/js/mdb.js"></script>
	
<script>
  new WOW().init();
</script>
	
</body>
</html>